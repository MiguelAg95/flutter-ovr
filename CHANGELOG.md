# [0.0.1] - 2020/11/06

Initial version of the library.

- Includes OVRValidator, a Widget that makes a call every 5 seconds (adjustable) to verify the project's state.