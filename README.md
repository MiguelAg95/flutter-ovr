# Flutter OVR

Flutter OVR is a validation system developed by OverCode Solutions that checks every X seconds if the project is active or suspended due to non-payment.

## Usage

```dart
OVRValidator(
    projectSlug: 'ovr-testing',
    activeWidget: Scaffold(),
    suspendedWidget: MyOwnWidget(), // Black screen with text message if not specified
    refreshInterval: 10, // 5 seconds by default
)
```