import 'package:flutter/material.dart';

class OVRUnderMaintenanceMessage extends StatelessWidget {
  const OVRUnderMaintenanceMessage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Volvemos enseguida',
              textScaleFactor: 1.6,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            Text(
              'Estamos realizando tareas de mantenimiento, disculpa las molestias.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
    );
  }
}
