import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'ovr_api_client.dart';
import 'ovr_project.dart';
import 'ovr_suspended_message.dart';
import 'ovr_under_maintenance_message.dart';

class OVRValidator extends StatefulWidget {
  final String projectSlug;
  final Duration refreshInterval;
  final Widget activeWidget;
  final Widget? suspendedWidget;
  final Widget? underMaintenanceWidget;

  OVRValidator({
    required this.projectSlug,
    this.refreshInterval = const Duration(seconds: 5),
    required this.activeWidget,
    this.suspendedWidget,
    this.underMaintenanceWidget,
  });

  @override
  _OVRValidatorState createState() => _OVRValidatorState();
}

class _OVRValidatorState extends State<OVRValidator> {
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: ovrAPIClient,
      child: Query(
        options: QueryOptions(
          document: gql(getOVRProjectQuery(widget.projectSlug)),
          pollInterval: widget.refreshInterval,
        ),
        builder: (
          QueryResult result, {
          VoidCallback? refetch,
          FetchMore? fetchMore,
        }) {
          if (!result.hasException && result.data != null) {
            OVRProject ovrProject = OVRProject.fromJson(result.data!);
            if (ovrProject.status == 'SUSPENDED') {
              return widget.suspendedWidget ?? OVRSuspendedMessage();
            }

            if (ovrProject.status == 'UNDER_MAINTENANCE') {
              return widget.underMaintenanceWidget ??
                  OVRUnderMaintenanceMessage();
            }
          }
          return widget.activeWidget;
        },
      ),
    );
  }
}
