class OVRProject {
  final String status;

  const OVRProject(this.status);

  factory OVRProject.fromJson(Map<String, dynamic> json) {
    return OVRProject(json['project']['status'] as String);
  }
}
