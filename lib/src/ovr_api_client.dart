import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

const OVR_API = 'https://api.overcode.mx/graphql';

ValueNotifier<GraphQLClient> ovrAPIClient = ValueNotifier(
  GraphQLClient(
    link: HttpLink(OVR_API),
    cache: GraphQLCache(store: InMemoryStore()),
  ),
);

String getOVRProjectQuery(String projectSlug) {
  return """
    {
      project(slug: "$projectSlug") {
        status
      }
    }
  """;
}
